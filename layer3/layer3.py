import sys, random, logging, time
sys.path.append('../')
import layer2.layer2 as Layer2

"""
Layer3 does all of the network routing and ip scheming.
Layer3 assumes that all interfaces are 2 way. IE if a host has an interface to
connect to me, I must have an interface to connect to that host.
"""
class Layer3:
    def __init__(self, layer_4_cb):
        """Create a Layer 3 Object.

        `layer_4_cb` - Function. This layer will use `layer_4_cb` to pass data to Layer 4. `layer_4_cb`
                       must accept a single parameter `data`.
        """        
        #Save layer 4 callback, connect to Layer 2, initialize lookup table,
        #tell upper layers that layer3 isn't ready yet, set up queue of seen
        #messages (to prevent infinite loops), and create a dictionary to map
        #neighbor interfaces to ip addresses
        self.layer_4_cb = layer_4_cb
        self.layer2 = Layer2.Layer2(self.from_layer_2)
        self.lookup = {'xx':-1}
        self.ready = False
        self.recent_messages = []
        self.neighbors = {}
        self.timer = time.time() - 100
        
        #Get user type and act accordingly
        self.type = "ro"
        if("--utype=dhcp" in sys.argv):
            self.type = "dh"
            self.ip = "00"
            self.ip_array = ["00"]
            self.network_map = {"00":{}}
            self.counter = 4
            self.keys = []
        else:
            self.ip = "xx"
            if("--utype=potus" in sys.argv):
                self.type = "po"
                self.ip = "01"
            elif("--utype=weps" in sys.argv):
                self.type = "we"
                self.ip = "02"
            elif("--utype=ops" in sys.argv):
                self.type = "op"
                self.ip = "03"
            else:
                self.ip = "xx"
                while(self.ip == "xx"):
                    self.get_ip()
                    time.sleep(200)
            while(self.neighbors == {}):
                self.get_neighbors()
                time.sleep(100)
            while(self.lookup ==  {'xx':-1}):
                self.get_routes()
                time.sleep(200)
        self.ready = True

    def from_layer_4(self, data, dest_ip):
        """Call this function to send data to this layer from layer 4
        
        'data'  -   String. Data to be sent.
        'dest_ip'   -   String. Must be 2 characters, destination ip address.
        """
        logging.debug(f"Layer 3 received msg from Layer 4: {data}")

        #Send the message on the correct interface if possible
        if(dest_ip in self.lookup.keys()):
            self.layer2.from_layer_3(f"{dest_ip}ms{data}", self.lookup[dest_ip])
        else:
            self.layer2.from_layer_3(f"{dest_ip}ms{data}", -1)
    
    def get_neighbors(self):
        """Call this function to request every interface's ip address"""
        for interface in self.layer2.interfaces:
            self.layer2.from_layer_3(f"{self.ip}gn{interface}", interface)

    def get_routes(self):
        """Call this function to request a lookup table from the dhcp server"""
        data = '.'.join([f"{self.neighbors[i]}{i}" for i in self.neighbors])
        self.layer2.from_layer_3(f"00gr{self.ip}{data}", -1)

    def get_ip(self):
        self.key = random.randint(0, 10000)
        self.layer2.from_layer_3(f"00gi{self.key:04}", -1)

    def from_layer_2(self, data):
        """Call this function to send data to this layer from layer 2"""
        logging.debug(f"Layer 3 received msg from Layer 2: {data}")

        #Ignore the messages if you have already seen it more than 3 out of the last 10 times, 
        #or if it is the destination and equal to the last message seen
        if((time.time() - self.timer < 100) and (self.recent_messages.count(data) > 3 or (data[:2] == self.ip and self.recent_messages != [] and self.recent_messages[-1] == data))):
            return
        self.recent_messages.append(data)
        if(len(self.recent_messages) > 10):
            self.recent_messages = self.recent_messages[1:]

        self.timer = time.time()
        #If this message is not meant for this IP, pass it on
        if(data[:2] != self.ip):
            #Get neighbors is the exception because the incoming ip does not know the destination ip
            if(data[2:4] == "gn"):
                for interface in self.layer2.interfaces:
                    self.layer2.from_layer_3(f"{data[:2]}s1{self.ip}{data[4:]}.{interface}", interface)
            #If you know the interface, send to that one, otherwise send to all
            elif(data[:2] in self.lookup.keys()):
                self.layer2.from_layer_3(data, self.lookup[data[:2]])
            elif(data[2] != "s"):
                self.layer2.from_layer_3(data, -1)
        elif(self.type == "dh"):
            if(data[2:4] == "gi"):
                if(not data[6:10] in self.keys):
                    self.keys.append(data[4:8])
                    self.ip_array.append(f"{self.counter:02}")
                    self.layer2.from_layer_3(f"xxmi{data[4:8]}{self.counter:02}", -1)
                    self.counter += 1
            elif(data[2:4] == "gr"):
                if(data[4:6] == "01" and not "01" in self.ip_array):
                    self.ip_array.append("01")
                elif(data[4:6] == "02" and not "02" in self.ip_array):
                    self.ip_array.append("02")
                elif(data[4:6] == "03" and not "03" in self.ip_array):
                    self.ip_array.append("03")
                self.dhcp_route(data)
            elif(data[2:4] == "s2"):
                self.neighbors[int(data[6:])] = data[4:6]
                self.network_map["00"][data[4:6]] = data[6:]
        else:
            if(data[2:4] == "ms"):
                self.layer_4_cb(data[4:])
            elif(data[2:4] == "s1"):
                self.neighbors[int(data.split('.')[0][6:])] = data[4:6]
                self.layer2.from_layer_3(f"{data[4:6]}s2{self.ip}{data.split('.')[1]}", int(data.split('.')[0][6:]))
                self.get_routes()
            elif(data[2:4] == "s2"):
                self.neighbors[int(data[6:])] = data[4:6]
                self.get_routes()
            elif(data[2:4] == "mr"):
                self.lookup = {}
                for i in data[4:].split('.'):
                    self.lookup[i[:2]] = int(i[2:])
            elif(data[2:4] == "mi" and self.key == int(data[4:8])):
                self.ip = data[8:10]

    def dhcp_route(self, data):
        self.network_map[data[4:6]] = {}
        for i in data[6:].split('.'):
            self.network_map[data[4:6]][i[:2]] = i[2:]
        for i in self.network_map.keys():
            look = {}
            seen = [i]
            queue = [(j, self.network_map[i][j]) for j in self.network_map[i].keys()]
            while(len(queue) > 0):
                if (not queue[0][0] in seen):
                    seen.append(queue[0][0])
                    look[queue[0][0]] = int(queue[0][1])
                    if(queue[0][0] in self.network_map.keys()):
                        for j in self.network_map[queue[0][0]].keys():
                            queue.append((j, queue[0][1]))
                queue.pop(0)
            if(i == "00"):
                self.lookup = look
            else:
                new_dat = '.'.join([j + str(look[j]) for j in look.keys()])
                if(i in self.lookup.keys()):
                    self.layer2.from_layer_3(f"{i}mr{new_dat}", self.lookup[i])
                else:
                    self.layer2.from_layer_3(f"{i}mr{new_dat}", -1)

