import sys, logging
import layer1.EdgeCodesLayer1 as Layer1
sys.path.append('../')

def parity(int_list):
    """Outputs 2D parity of a list of ones and zeroes. 

    'int_list' - Int[]. List of 1s and 0s. len(int_list) % 8 must = 0.
    """
    nested_int_list = [int_list[i:i+8] for i in range (0,len(int_list),8)]
    horizontal = [chunk.count(1)%2 for chunk in nested_int_list]
    vertical = [[chunk[j] for chunk in nested_int_list].count(1)%2 for j in range(8)]
    return horizontal + vertical

class Layer2:
    def __init__(self, layer_3_cb):
        """Create a Layer 2 Object.

        `layer_3_cb` - Function. This layer will use `layer_3_cb` to pass data to Layer 3. `layer_3_cb`
                       must accept a single parameter `data`.
        """
        # Save layer 3 callback function to send data back up later
        self.layer_3_cb = layer_3_cb

        #Create layer1 interfaces to connect to
        self.interfaces = {}
        for i in sys.argv[1:]:
            if(i[:7] == "--input"):
                interface_num = int(i.split('=')[0][7:])
                self.interfaces[interface_num] = Layer1.EdgeCodesLayer1(interface_num, self.from_layer_1, True)

        #Initialize preamble and end flag to differentiate messages from noise
        self.preamble = [1,0,1,0,1,0,1,0]*3 + [1,0,1,0,1,0,1,1]*3
        self.end_flag = [1,1,1,0,1,1,1,0]*3 + [1,0,1,0,1,1,1,1]*3

        #Initialize buffer to store messages, max_size for sending size
        self.buffer = []
        self.max_size = 1024
    
    def parse_packet(self, data):
        """Call this function to error check and pass layer 3 a packet.

        'data'  -   int[]. A list of 1s and 0s. Preamble and end_flag must be
                    removed beforehand.
        """
        #Ensure that the packet is a valid length
        pack_size = (len(data) - 8) / 9 * 8
        if (pack_size != int(pack_size)):
            return
        pack_size = int(pack_size)

        #Make sure that the parity checks out
        packet = data[:pack_size]
        par = data[pack_size:]
        check = parity(packet)

        #If it doesn't try to error correct, Error correction only works if
        #there is one flipped bit, it does not correct if there are more flipped
        #bits, if there is a bit dropped, or a bit added.
        #If there is an error, do nothing
        if(par != check):
            horizontal = par[:-8]
            horizontal_check = check[:-8]
            vertical = par[-8:]
            vertical_check = check[-8:]
            index_h = None

            #Check parity to see if one flipped bit
            for i in range(len(horizontal)):
                if(horizontal[i] != horizontal_check[i]):
                    if(index_h == None):
                        index_h = i
                    else:
                        return
            index_v = None
            for i in range(8):
                if(vertical[i] != vertical_check[i]):
                    if(index_v == None):
                        index_v = i
                    else:
                        return

            #If only one flipped bit, flip it back
            if(index_v != None and index_h != None):
                packet[index_h * 8 + index_v] = (packet[index_h * 8 + index_v] + 1) % 2

        #Convert binary to ascii and pass up to layer 3
        self.layer_3_cb(''.join([chr(int(''.join([str(j) for j in packet[i:i+8]]), 2)) for i in range(0, len(packet), 8)]))

    def from_layer_3(self, data, interface):
        """Call this function to send data to this layer from layer 3
        
        'data'  -   String. Data to be sent.
        'interface' -   Int. Which interface to send the data to.
        """
        logging.debug(f"Layer 2 received msg from Layer 3: {data}")

        #This uses recursion to limit data size to max_size
        if(len(data)*8 > self.max_size):
            self.from_layer_3(data[:self.max_size // 8], interface)
            self.from_layer_3(data[self.max_size // 8:], interface)
            return

        #Make data binary, add preamble, 2d parity, and end flag
        data = [int(j) for j in ''.join([format(ord(i), '08b') for i in data])]
        data = self.preamble + data + parity(data) + self.end_flag

        #Send data out on proper interface if available, -1 is broadcast
        if(interface == -1):
            for i in self.interfaces: 
                self.interfaces[i].from_layer_2(data)
        elif(interface in self.interfaces.keys()):
            self.interfaces[interface].from_layer_2(data)

    def from_layer_1(self, data):
        """Call this function to send data to this layer from layer 1

        'data'  -   int[]. List of 1s and 0s received by layer 1.
        """
        logging.debug(f"Layer 2 received msg from Layer 1: {data}") 

        #Maximum possible packet size
        max_pack = int(len(self.preamble) + self.max_size * 9 / 8 + 8 + len(self.end_flag))

        #Add incoming data to buffer, weed out random noise with preamble
        self.buffer += data
        size_min = min(len(self.buffer), len(self.preamble))
        while(self.buffer[:size_min] != self.preamble[:size_min]):
            self.buffer = self.buffer[1:]
            size_min = min(len(self.buffer), len(self.preamble))

        #Check packet for end flag if it is long enough
        if(len(self.buffer) > len(self.preamble) + len(self.end_flag)):

            #Look for end_flag
            for i in range(len(self.preamble), min(max_pack, len(self.buffer)) - len(self.end_flag)):
                if(self.buffer[i + 1:i + 1 + len(self.end_flag)] == self.end_flag):
                    self.parse_packet(self.buffer[len(self.preamble):i+1])
                    self.buffer = self.buffer[i+1+len(self.end_flag):]
                    self.from_layer_1([])
                    return

            #Skip packet if too long and missing end_flag
            if(len(self.buffer) > max_pack):
                self.buffer = self.buffer[1:]
                self.from_layer_1([])
