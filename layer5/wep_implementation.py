class Weapon:
    launchCode = "9999"
    def __init__(self) -> None:
        self.launchState = "PASSIVE"
        self.lat = None
        self.lon = None
    
    def upgradeLaunchState(self):
        if self.launchState == "PASSIVE":
            self.launchState = "STANDBY"
            self.conductStandbyProcedure()
        elif self.launchState == "STANDBY":
            self.launchState = "TARGETED"
        elif self.launchState == "TARGETED":
            self.launchState = "FIRED"
    
    def getLaunchState(self):
        return self.launchState
    def conductStandbyProcedure(self):
        print("\u2772Conducting weapon standby procedures\u2773")
    def setTargets(self, lat, lon):
        print(f"\u2772Setting latitude to {lat} and longitude to {lon}\u2773")
        self.lat = lat
        self.lon = lon
    def fireWeapon(self, code):
        if code == Weapon.launchCode:
            print("\u2739 BOOM \u2739")
            return True
        else:
            return False
