from threading import Thread

import sys, logging, time
sys.path.append('../')
import layer4.layer4 as Layer4
logging.basicConfig(filename="logging.log", level=logging.DEBUG)

terminal_type = "router"
for i in sys.argv[1:]:
    if('--utype=' == i[:8]):
        terminal_type = i[8:]

layer4 = Layer4.Layer4()
def ops():
    choice = -1
    chat = []
    def chats(data):
        chat.append(data)
    wep = []
    def weps(data):
        wep.append(data)
    def printer():
        while(True):
            if(len(wep) > 0 and choice == 1):
                print(wep.pop(0))
            if(len(chat) > 0 and choice == 0):
                print(f"POTUS> {chat.pop(0)}")
    layer4.connect_to_socket(weps, 100)
    layer4.connect_to_socket(chats, 80)
    Thread(target=printer, args=()).start()
    while(True):
        choice = -1
        while(choice != 0 and choice != 1):
            try:
                print("What do you want to do?")
                print("0: Join president chat")
                print("1: Issue weps command")
                choice = int(input())
            except:
                print("Improper command")
        if(choice == 0):
            print("Enter 'Q' to quit the chat")
            send = ""
            while(True):
                send = input()
                if(send == "Q"):
                    break
                layer4.from_layer_5(send, 80, 80, "01")
        elif(choice == 1):
            print("Enter 'Q' to quit the command prompt")
            print("Commands are 'STATUS', 'INITIALIZE SEQUENCE', 'SET COORDINATES (lat) (long)', 'LAUNCH', and 'CANCEL'")
            send = ""
            while(True):
                send = input()
                if(send == "Q"):
                    break
                layer4.from_layer_5(send, 100, 100, "02")

def weps():
    commands = []
    def recv_cmd(data):
        commands.append(data)
    layer4.connect_to_socket(recv_cmd, 100)
    status = 0
    lat = None
    lon = None
    while(True):
        if(len(commands) > 0):
            data = commands.pop(0)
            if(data == "STATUS"):
                response = ""
                if(status == 0):
                    response += "SEQUENCE NOT INITIALIZED"
                elif(status == 1):
                    response += "SEQUENCE INITIALIZED"
                else:
                    response += "MISSILES LAUNCHED"
                if(lat == None):
                    response += " - NO COORDINATES"
                else:
                    response += f" - MISSILES AIMED AT LAT: {lat} LON: {lon}"
                layer4.from_layer_5(response, 100, 100, "03")
            elif(data == "INITIALIZE SEQUENCE"):
                status = 1
                layer4.from_layer_5("SEQUENCE INITIALIZED", 100, 100, "03")
            elif(data == "LAUNCH"):
                if(status == 1):
                    if(lat == None or lon == None):
                        layer4.from_layer_5("CANNOT LAUNCH, NO COORDINATES", 100, 100, "03")
                    else:
                        status = 2
                        layer4.from_layer_5("MISSILES LAUNCHED", 100, 100, "03")
                else:
                    layer4.from_layer_5("CANNOT LAUNCH, SEQUENCE NOT INITIALIZED", 100, 100, "03")
            elif(data == "CANCEL"):
                if(status == 2):
                    layer4.from_layer_5("TOO LATE, MISSLES LAUNCHED", 100, 100, "03")
                else:
                    status = 0
            elif(data[:16] == "SET COORDINATES "):
                if(len(data.split()) == 4):
                    try:
                        if(abs(float(data.split()[2])) > 90 or abs(float(data.split()[3])) > 180):
                            layer4.from_layer_5("IMPROPER COORDINATE RANGE", 100, 100, "03")
                        else:
                            lat = float(data.split()[2])
                            lon = float(data.split()[3])
                    except:
                        layer4.from_layer_5("FORMAT ERROR", 100, 100, "03")
            else:
                layer4.from_layer_5("FORMAT ERROR", 100, 100, "03")

def potus():
    def chat(data):
        print(f"OPS> {data}")
    layer4.connect_to_socket(chat, 80)
    print("Welcome to the chat POTUS")
    while(True):
        layer4.from_layer_5(input(), 80, 80, "03")

def router():
    while(True):
        time.sleep(100)


if(terminal_type == "potus"):
    Thread(target=potus, args=()).start()
elif(terminal_type == "weps"):
    Thread(target=weps, args=()).start()
elif(terminal_type == "ops"):
    Thread(target=ops, args=()).start()
else:
    Thread(target=router, args=()).start()
