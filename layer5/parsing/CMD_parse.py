from datetime import datetime

def standby(sender, TSID, t):
    now = datetime.now()
    nowS = str(now)
    parts = t.split(' ')
    r = "CMD\nsender: " + sender + "\nTSID: " + TSID + "\ntimesent: " + nowS + "\ntype: " + parts[0][1:]
    return r

def target(sender, TSID, t):
    now = datetime.now()
    nowS = str(now)
    parts = t.split(' ')
    r = "CMD\nsender: " + sender + "\nTSID: " + TSID + "\ntimesent: " + nowS + "\ntype: " + parts[0][1:] + "\nlat: " + parts[2][5:] + "\nlon: " + parts[3][5:]
    return r

def fire(sender, TSID, t):
    now = datetime.now()
    nowS = str(now)
    parts = t.split(' ')
    r = "CMD\nsender: " + sender + "\nTSID: " + TSID + "\ntimesent: " + nowS + "\ntype: " + parts[0][1:] + "\nlaunch: " + parts[2][8:]
    return r



'''
sender = "OPS"
textStandby = "*STANDBY weapon1"
textTarget = "*TARGET weapon1 -lat=S.65.42.11 -lon=E.123.33.14"
textFire = "*FIRE weapon1 -launch=O@I^18u^UhbN3onV6oxVMqWK5voo3l"
print(standby(textStandby))
print(target(textTarget))
print(fire(textFire))
'''
