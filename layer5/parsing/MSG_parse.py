from datetime import datetime

def creatMsg(sender, plaintxt, tsid):
  unparssedMsg = "MSG\nsender: "
  senderFlag = checkInput(sender)

  if(senderFlag):
    unparssedMsg = unparssedMsg+sender+"\n"
  else:
    unparssedMsg = unparssedMsg+"FAIL"+"\n"

  tsidFlag = checkInput(tsid)
  if(tsidFlag):
    unparssedMsg = unparssedMsg+"TSID: "+tsid
  else:
    unparssedMsg = unparssedMsg+"TSID: FAIL"

  plaintxtFlag = checkInput(plaintxt)

  now = datetime.now()

  current_time = now.strftime("%H:%M:%S")


  unparssedMsg = unparssedMsg+"\ntimesent: "+current_time

  if(plaintxtFlag):
    unparssedMsg = unparssedMsg+"\nplaintext: "+plaintxt
  else:
    unparssedMsg = unparssedMsg+"\nplaintext: FAIL"

  error = checkForError(senderFlag, tsidFlag, plaintxtFlag)
  unparssedMsg = unparssedMsg+error
  return unparssedMsg


def checkInput (str):
    return (str!="")

def checkForError (senderFlag, tsidFlag, plaintxtFlag ):
    error = ""
    if(bool(senderFlag) and bool(tsidFlag) and bool(plaintxtFlag)):
        error = ""
    else:
        error= error+"\nMissing  "
        if(bool(senderFlag)==False):
            error = error+"sender"
            raise Exception("SENDER NOT SENT")

        if(bool(tsidFlag)==False):
            raise Exception("TSID NOT SENT")

        if(bool(plaintxtFlag)==False):
                raise Exception("PLAINTEXT NOT SENT")

    return error

def returnInfo(unparssedMsg):
  fields = unparssedMsg.split('\n')
  user = fields[1].split()[1] 
  TSID = fields[2].split()[1]
  time = fields[3].split()[1]
  msg_contents = fields[4]

  return user, TSID, time, msg_contents


#creatMsg("queen", "testing 1,2,3 ...", "asdfuwe9857rzjkhsdrjh12;klqwj@(*#7)")
